/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.robotproject2;

import java.util.Scanner;

/**
 *
 * @author Nine
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        {
            Tablemap map = new Tablemap(10, 10);
            Robot robot = new Robot(2, 2, 'x', map);
            Bomb bomb = new Bomb(5, 5);
            map.setRobot(robot);
            map.setBomb(bomb);
            while (true) {
                map.showMap();
                char direction=inputDirection(kb);
                if(direction =='q'){
                    printByeBye();
                    break;
                }
                robot.walk(direction);
            }
        }
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner kb) {
        String str=kb.next();
        return str.charAt(0);
    }
}
